import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl, AbstractControl } from "@angular/forms";
import Web3  from "web3";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  searchForm: FormGroup;  
  title = 'app works!';

  constructor(private fb: FormBuilder) {    
  }

  ngOnInit() {
          this.searchForm = this.fb.group({
          "GridPrice": "", 
          "MarketSupply": "",
          "LocalSupply": "",
          "MarketDemand": "",
          "LocalDemand": "",
      });
  }

    onSubmit(){
    }

}
