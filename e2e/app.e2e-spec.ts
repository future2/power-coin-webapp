import { PowercoinPage } from './app.po';

describe('powercoin App', () => {
  let page: PowercoinPage;

  beforeEach(() => {
    page = new PowercoinPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
