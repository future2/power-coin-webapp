import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material'
import { AppComponent } from './app.component';
import { BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { BuySellSettingsComponent } from './buy-sell-settings/buy-sell-settings.component';
import { DashComponent } from './dash/dash.component';
import { MyWalletComponent } from './my-wallet/my-wallet.component';
import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: 'dash', component: DashComponent },
  { path: 'mywallet', component: MyWalletComponent },
  { path: 'buysell',component: BuySellSettingsComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    BuySellSettingsComponent,
    DashComponent,
    MyWalletComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpModule,
    MaterialModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
