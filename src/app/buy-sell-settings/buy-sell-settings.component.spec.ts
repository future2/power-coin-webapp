import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuySellSettingsComponent } from './buy-sell-settings.component';

describe('BuySellSettingsComponent', () => {
  let component: BuySellSettingsComponent;
  let fixture: ComponentFixture<BuySellSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuySellSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuySellSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
